'use strict';

const api = 'https://swapi.co/api';
const charactersAPI = `${api}/starships`;

let promise = fetch(charactersAPI);

promise.then(function (response) {
	response.json().then(function (result) {
		parseResult(result);
	});
}).catch(function(e) {
	console.log(e);
});

function parseResult(response) {
	let container = document.querySelector('#starshipCards');

	response.results.forEach( starship => {
		let titleInfo = document.createElement('div');
		titleInfo.setAttribute('class', 'title-info');

		let titleCard = document.createElement('div');
		titleCard.setAttribute('class', 'title-card');

		let titleName = document.createElement('p');
		titleName.textContent = ('Name:');

		let titleStarshipClass = document.createElement('p');
		titleStarshipClass.textContent = ('Starship-class:');

		let titleLength = document.createElement('p');
		titleLength.textContent = ('Starship-length:');

		let titlePassengers = document.createElement('p');
		titlePassengers.textContent = ('Passengers:');

		let titlePrice = document.createElement('p');
		titlePrice.textContent = ('Price:');


		let card = document.createElement('div');
		card.setAttribute('class', 'starship-card');
		
		let name = document.createElement('p');
		name.textContent = starship.name;

		let starshipClass = document.createElement('p');
		starshipClass.setAttribute('class', 'starship-class');
		starshipClass.textContent = starship.starship_class;

		let length = document.createElement('p');
		length.setAttribute('class', 'starship-length');
		length.textContent = starship.length;

		let passengers = document.createElement('p');
		passengers.setAttribute('class', 'starship-passengers');
		passengers.textContent = starship.passengers;

		let price = document.createElement('p');
		price.setAttribute('class', 'starship-price');
		price.textContent = starship.cost_in_credits;

		container.appendChild(titleInfo);
		
		titleCard.appendChild(titleName);
		titleCard.appendChild(titleStarshipClass);
		titleCard.appendChild(titleLength);
		titleCard.appendChild(titlePassengers);
		titleCard.appendChild(titlePrice);

		titleInfo.appendChild(titleCard);
		titleInfo.appendChild(card);

		card.appendChild(name);
		card.appendChild(starshipClass);
		card.appendChild(length);
		card.appendChild(passengers);
		card.appendChild(price);
	});
};